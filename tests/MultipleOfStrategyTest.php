<?php

declare(strict_types=1);

namespace App\Tests;

use App\Strategy\MultipleOfStrategy;
use App\Strategy\MultipleStrategy;
use PHPUnit\Framework\TestCase;

final class MultipleOfStrategyTest extends TestCase
{
    /**
     * @dataProvider multipleOfProvider
     *
     * @param int              $value
     * @param MultipleStrategy $multipleStrategy
     */
    public function testItIsMultipleOf(int $value, MultipleStrategy  $multipleStrategy): void
    {
        $this->assertTrue($multipleStrategy->isMultiple($value));
    }

    /**
     * @dataProvider notMultipleOfProvider
     *
     * @param int              $value
     * @param MultipleStrategy $multipleStrategy
     */
    public function testItIsNotMultipleOf(int $value, MultipleStrategy  $multipleStrategy): void
    {
        $this->assertFalse($multipleStrategy->isMultiple($value));
    }

    public function multipleOfProvider(): array
    {
        $multipleOf3Strategy = new MultipleOfStrategy(3);
        $multipleOf5Strategy = new MultipleOfStrategy(5);

        return [
            [3, $multipleOf3Strategy],
            [9, $multipleOf3Strategy],
            [12, $multipleOf3Strategy],
            [15, $multipleOf3Strategy],
            [33, $multipleOf3Strategy],
            [99, $multipleOf3Strategy],
            [5, $multipleOf5Strategy],
            [15, $multipleOf5Strategy],
            [25, $multipleOf5Strategy],
            [50, $multipleOf5Strategy],
            [100, $multipleOf5Strategy],
        ];
    }

    public function notMultipleOfProvider(): array
    {
        $multipleOf3Strategy = new MultipleOfStrategy(3);
        $multipleOf5Strategy = new MultipleOfStrategy(5);

        return [
            [3, $multipleOf5Strategy],
            [9, $multipleOf5Strategy],
            [12, $multipleOf5Strategy],
            [33, $multipleOf5Strategy],
            [99, $multipleOf5Strategy],
            [5, $multipleOf3Strategy],
            [25, $multipleOf3Strategy],
            [50, $multipleOf3Strategy],
            [100, $multipleOf3Strategy],
        ];
    }
}