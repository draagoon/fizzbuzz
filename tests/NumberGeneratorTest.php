<?php

declare(strict_types=1);

namespace App\Tests;

use App\Exception\NumberException;
use App\Generator\NumberGenerator;
use App\Limit\GreaterOrEqualNumberLimit;
use PHPUnit\Framework\TestCase;

final class NumberGeneratorTest extends TestCase
{
    /**
     * @dataProvider generatorProvider
     *
     * @param NumberGenerator $numberGenerator
     * @param int[]           $range
     */
    public function testItGenerates(NumberGenerator $numberGenerator, array $range): void
    {
        $this->assertEquals($range, iterator_to_array($numberGenerator->generate()));
    }

    public function generatorProvider(): array
    {
        return [
            [
                new NumberGenerator(0, new GreaterOrEqualNumberLimit(10)),
                range(0, 10)
            ],
            [
                new NumberGenerator(0, new GreaterOrEqualNumberLimit(10), 2),
                range(0, 10, 2)
            ]
        ];
    }

    public function testItWorksOnlyIfStartIsLowerThanLimit(): void
    {
        $this->expectException(NumberException::class);
        $this->expectExceptionMessage(sprintf(NumberException::START_IS_GREATER_THAN_LIMIT, 10, 2));

        new NumberGenerator(10, new GreaterOrEqualNumberLimit(2));
    }

    public function testNegativeRangesArentAllowed(): void
    {
        $this->expectException(NumberException::class);
        $this->expectExceptionMessage(NumberException::NEGATIVE_NUMBERS_ARENT_ALLOWED);

        new NumberGenerator(-2, new GreaterOrEqualNumberLimit(-10), -2);
    }

    public function testEndlessLoopsArentAllowed(): void
    {
        $this->expectException(NumberException::class);
        $this->expectExceptionMessage(NumberException::ENDLESS_LOOPS);

        new NumberGenerator(1, new GreaterOrEqualNumberLimit(10), 0);
    }

    public function testNoLoop(): void
    {
        $this->expectException(NumberException::class);
        $this->expectExceptionMessage(sprintf(NumberException::NO_RANGE_CAN_BE_GENERATED, 10));

        new NumberGenerator(10, new GreaterOrEqualNumberLimit(10));
    }
}