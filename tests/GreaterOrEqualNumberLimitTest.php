<?php

declare(strict_types=1);

namespace App\Tests;

use App\Limit\GreaterOrEqualNumberLimit;
use PHPUnit\Framework\TestCase;

final class GreaterOrEqualNumberLimitTest extends TestCase
{
    /**
     * @dataProvider lowerThanValueProvider
     *
     * @param int $value
     * @param int $limitValue
     */
    public function testItIsLowerThan(int $value, int $limitValue): void
    {
        $limit = new GreaterOrEqualNumberLimit($limitValue);
        $this->assertTrue($limit->isLowerThan($value));
    }

    /**
     * @dataProvider notLowerThanValueProvider
     *
     * @param int $value
     * @param int $limitValue
     */
    public function testItIsNotLowerThan(int $value, int $limitValue): void
    {
        $limit = new GreaterOrEqualNumberLimit($limitValue);
        $this->assertFalse($limit->isLowerThan($value));
    }

    /**
     * @dataProvider greaterThanValueProvider
     *
     * @param int $value
     * @param int $limitValue
     */
    public function testItIsGreaterThan(int $value, int $limitValue): void
    {
        $limit = new GreaterOrEqualNumberLimit($limitValue);
        $this->assertTrue($limit->isGreaterThan($value));
    }

    /**
     * @dataProvider isValidValueProvider
     *
     * @param int $value
     * @param int $limitValue
     */
    public function testItIsValid(int $value, int $limitValue): void
    {
        $limit = new GreaterOrEqualNumberLimit($limitValue);
        $this->assertTrue($limit->isValid($value));
    }

    public function lowerThanValueProvider(): array
    {
        return [
            [1, 0],
            [100, 99],
            [PHP_INT_MAX, PHP_INT_MAX - 1],
        ];
    }

    public function notLowerThanValueProvider(): array
    {
        return [
            [0, 0],
            [42, 100],
            [PHP_INT_MAX, PHP_INT_MAX],
        ];
    }

    public function greaterThanValueProvider(): array
    {
        return [
            [0, 1],
            [42, 100],
            [PHP_INT_MAX - 1, PHP_INT_MAX],
        ];
    }

    public function isValidValueProvider(): array
    {
        return [
            [0, 1],
            [42, 100],
            [PHP_INT_MAX - 1, PHP_INT_MAX],
        ];
    }
}