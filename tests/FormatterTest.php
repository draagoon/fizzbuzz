<?php

declare(strict_types=1);

namespace App\Tests;

use App\Exception\NumberException;
use App\Formatter\BuzzFormatter;
use App\Formatter\DefaultNumberFormatter;
use App\Formatter\FizzBuzzFormatter;
use App\Formatter\FizzFormatter;
use App\Formatter\NumberFormatter;
use App\Strategy\AllMultipleOfStrategy;
use App\Strategy\MultipleOfStrategy;
use PHPUnit\Framework\TestCase;

final class FormatterTest extends TestCase
{
    /**
     * @dataProvider formatsIntoProvider
     *
     * @param int             $value
     * @param string          $word
     * @param NumberFormatter $formatter
     */
    public function testItFormatsInto(int $value, string $word, NumberFormatter $formatter): void
    {
        $this->assertEquals($word, $formatter->format($value));
    }

    public function testItCannotBeFormatted(): void
    {
        $this->expectException(NumberException::class);
        $this->expectExceptionMessage('It is not possible to format the number 1');

        $fizz = new FizzFormatter(new MultipleOfStrategy(3));
        $fizz->format(1);
    }

    public function formatsIntoProvider(): array
    {
        $defaultNumberFormatter = new DefaultNumberFormatter();
        $multipleOf1Strategy    = new MultipleOfStrategy(1);
        $multipleOf3Strategy    = new MultipleOfStrategy(3);
        $multipleOf5Strategy    = new MultipleOfStrategy(5);

        $fizzbuzz = new FizzBuzzFormatter(
            new AllMultipleOfStrategy($multipleOf3Strategy, $multipleOf5Strategy)
        );
        $buzz     = new BuzzFormatter($multipleOf5Strategy);
        $fizz     = new FizzFormatter($multipleOf3Strategy);
        $default  = new DefaultNumberFormatter();

        $fizzbuzz->setNextFormatter($buzz);
        $buzz->setNextFormatter($fizz);
        $fizz->setNextFormatter($default);

        return [
            [0, '0', $defaultNumberFormatter],
            [0, 'Buzz', new BuzzFormatter($multipleOf1Strategy)],
            [0, 'Buzz', new BuzzFormatter($multipleOf3Strategy)],
            [0, 'Buzz', new BuzzFormatter($multipleOf5Strategy)],
            [0, 'Fizz', new FizzFormatter($multipleOf3Strategy)],
            [0, 'Fizz', new FizzFormatter($multipleOf5Strategy)],
            [0, 'FizzBuzz', new FizzBuzzFormatter($multipleOf3Strategy)],
            [0, 'FizzBuzz', new FizzBuzzFormatter($multipleOf5Strategy)],
            [1, 'Buzz', new BuzzFormatter($multipleOf1Strategy)],
            [1, '1', $fizzbuzz],
            [2, '2', $fizzbuzz],
            [3, 'Fizz', $fizzbuzz],
            [5, 'Buzz', $fizzbuzz],
            [6, 'Fizz', $fizzbuzz],
            [9, 'Fizz', $fizzbuzz],
            [10, 'Buzz', $fizzbuzz],
            [11, '11', $fizzbuzz],
            [12, 'Fizz', $fizzbuzz],
            [14, '14', $fizzbuzz],
            [15, 'FizzBuzz', $fizzbuzz],
            [18, 'Fizz', $fizzbuzz],
            [20, 'Buzz', $fizzbuzz],
            [25, 'Buzz', $fizzbuzz],
            [30, 'FizzBuzz', $fizzbuzz],
            [33, 'Fizz', $fizzbuzz],
            [50, 'Buzz', $fizzbuzz],
            [90, 'FizzBuzz', $fizzbuzz],
            [100, 'Buzz', $fizzbuzz],
        ];
    }
}