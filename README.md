# Fizz-Buzz

Die gesamte Applikation läuft per Docker, so dass (außer Docker & ggf. make) keine Abhängigkeiten installiert sein müssen.
Zunächst muss allerdings eine `.env` mit der `USER_ID` des Systems angelegt werden. Default ist `1000`.
Dazu kann die `.env.test` einfach kopiert werden per `cp .env.test .env`. Danach muss per `make new` die Applikation einmal initial gebaut werden.

 - Per `make new` werden die Container initial gebaut.
 - Per `make kill` werden die Container gestoppt und zerstört.
 - Per `make run` oder `docker-compose up -d && docker-compose exec php php bin/console app:fizzbuzz [<limit> [<start>]]` kann die Ausgabe des Fizz-Buzz Commands angesehen werden.
   - `<limit>` ist die maximale (inklusive) Zahl, die formatiert und ausgegeben wird. Standard ist `100`
   - `<start>` ist die minimale (inklusive) Zahl, die formatiert und ausgegeben wird. Standard ist `1`
 - Per `make analyse` oder `docker-compose up -d && docker-compose exec php composer analyse` werden `phplint`, `phpstan` und `phan` ausgeführt.
 - Per `make test` oder `docker-compose up -d && docker-compose exec php composer test` werden die Tests ausgeführt.

Die Ausgabe kann - nach dem Starten der Docker-Container - auch im Browser aufgerufen werden unter http://localhost:8080
