<?php

declare(strict_types=1);

namespace App\Service;

use App\Formatter\BuzzFormatter;
use App\Formatter\DefaultNumberFormatter;
use App\Formatter\FizzBuzzFormatter;
use App\Formatter\FizzFormatter;
use App\Formatter\Service\NumberFormatterService;
use App\Generator\NumberGenerator;
use App\Limit\GreaterOrEqualNumberLimit;
use App\Strategy\AllMultipleOfStrategy;
use App\Strategy\MultipleOfStrategy;

/**
 * Class FizzBuzzService
 * @package App\Service
 */
final class FizzBuzzService
{
    /**
     * @var NumberFormatterService
     */
    private $service;

    /**
     * FizzBuzzService constructor.
     *
     * @param int $start
     * @param int $limit
     * @param int $step
     */
    public function __construct(int $start, int $limit, int $step = 1)
    {
        $this->service = new NumberFormatterService(
            new NumberGenerator(
                $start,
                new GreaterOrEqualNumberLimit($limit),
                $step
            ),
            self::buildFormatter()
        );
    }

    /**
     * @return iterable|string[]
     */
    public function generate(): iterable
    {
        return $this->service->formatAllGenerated();
    }

    /**
     * @return FizzBuzzFormatter
     */
    private static function buildFormatter(): FizzBuzzFormatter
    {
        $multipleOf3Strategy = new MultipleOfStrategy(3);
        $multipleOf5Strategy = new MultipleOfStrategy(5);

        $fizzbuzz = new FizzBuzzFormatter(
            new AllMultipleOfStrategy($multipleOf3Strategy, $multipleOf5Strategy)
        );
        $buzz     = new BuzzFormatter($multipleOf5Strategy);
        $fizz     = new FizzFormatter($multipleOf3Strategy);
        $default  = new DefaultNumberFormatter();

        $fizzbuzz->setNextFormatter($buzz);
        $buzz->setNextFormatter($fizz);
        $fizz->setNextFormatter($default);

        return $fizzbuzz;
    }
}