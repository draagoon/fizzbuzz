<?php

declare(strict_types=1);

namespace App\Controller;

use App\Service\FizzBuzzService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

final class FizzBuzzController extends AbstractController
{
    private const START = 1;
    private const LIMIT = 100;
    private const STEP  = 1;

    public function index(): Response
    {
        $output  = [];
        $service = new FizzBuzzService(self::START, self::LIMIT, self::STEP);
        foreach ($service->generate() as $value) {
            $output[] = $value;
        }

        return $this->render('fizzbuzz.html.twig', ['output' => $output]);
    }
}