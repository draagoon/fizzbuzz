<?php

declare(strict_types=1);

namespace App\Command;

use App\Service\FizzBuzzService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class FizzBuzzCommand extends Command
{
    protected function configure(): void
    {
        $this->setName('app:fizzbuzz')
             ->setDescription('Numeric-Output with Fizz/Buzz output')
             ->addArgument(
                 'limit',
                 InputArgument::OPTIONAL,
                 'What should be the limit of the output? Default 100',
                 (string)100
             )->addArgument(
                'start',
                InputArgument::OPTIONAL,
                'Where should the counter start? Default 1',
                (string)1
            );
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $limit = $input->getArgument('limit');
        if (!is_numeric($limit)) {
            printf('Limit should be numeric, not %s' . PHP_EOL, var_export($limit, true));
            return 1;
        }

        $start = $input->getArgument('start');
        if (!is_numeric($start)) {
            printf('Start should be numeric, not %s' . PHP_EOL, var_export($start, true));
            return 1;
        }

        $service = new FizzBuzzService((int)$start, (int)$limit);
        foreach ($service->generate() as $value) {
            $output->writeln($value);
        }

        return 0;
    }
}