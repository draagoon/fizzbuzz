<?php

declare(strict_types=1);

namespace App\Generator;

use App\Exception\NumberException;
use App\Limit\NumberLimit;

/**
 * Class NumberGenerator
 * @package App\Generator
 */
final class NumberGenerator
{
    /**
     * @var int
     */
    private $start;
    /**
     * @var NumberLimit
     */
    private $limit;
    /**
     * @var int
     */
    private $step;

    /**
     * NumberGenerator constructor.
     *
     * @param int         $start
     * @param NumberLimit $limit
     * @param int         $step
     */
    public function __construct(int $start, NumberLimit $limit, int $step = 1)
    {
        if ($start < 0 || $step < 0 || $limit->isLowerThan(0)) {
            throw NumberException::negativeRangeIsNotAllowed();
        }

        if ($limit->isEqual($start)) {
            throw NumberException::startIsEqualToLimit($start);
        }

        if ($step === 0) {
            throw NumberException::stepIsZero();
        }

        if ($limit->isLowerThan($start)) {
            throw NumberException::startIsGreaterThanLimit($start, $limit->getValue());
        }

        $this->start = $start;
        $this->limit = $limit;
        $this->step  = $step;
    }

    /**
     * @return iterable|int[]
     */
    public function generate(): iterable
    {
        $value = $this->start;
        while ($this->limit->isValid($value)) {
            yield $value;
            $value += $this->step;
        }
    }
}