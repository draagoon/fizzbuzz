<?php

declare(strict_types=1);

namespace App\Limit;

/**
 * Class GreaterOrEqualNumberLimit
 * @package App\Limit
 */
final class GreaterOrEqualNumberLimit implements NumberLimit
{
    use NumberLimitImpl;

    /**
     * GreaterOrEqualNumberLimit constructor.
     *
     * @param int $limit
     */
    public function __construct(int $limit)
    {
        $this->limit = $limit;
    }

    /**
     * @param int $value
     *
     * @return bool
     */
    public function isValid(int $value): bool
    {
        return $this->limit >= $value;
    }
}