<?php

declare(strict_types=1);

namespace App\Limit;

/**
 * Interface NumberLimit
 * @package App\Limit
 */
interface NumberLimit
{
    /**
     * @return int
     */
    public function getValue(): int;

    /**
     * @param int $value
     *
     * @return bool
     */
    public function isValid(int $value): bool;

    /**
     * @param int $value
     *
     * @return bool
     */
    public function isEqual(int $value): bool;

    /**
     * @param int $value
     *
     * @return bool
     */
    public function isLowerThan(int $value): bool;

    /**
     * @param int $value
     *
     * @return bool
     */
    public function isGreaterThan(int $value): bool;
}
