<?php

declare(strict_types=1);

namespace App\Limit;

/**
 * Trait NumberLimitImpl
 * @package App\Limit
 */
trait NumberLimitImpl
{
    /**
     * @var int
     */
    private $limit = 0;

    /**
     * @return int
     */
    public function getValue(): int
    {
        return $this->limit;
    }

    /**
     * @param int $value
     *
     * @return bool
     */
    public function isEqual(int $value): bool
    {
        return $this->limit === $value;
    }

    /**
     * @param int $value
     *
     * @return bool
     */
    public function isLowerThan(int $value): bool
    {
        return $this->limit < $value;
    }

    /**
     * @param int $value
     *
     * @return bool
     */
    public function isGreaterThan(int $value): bool
    {
        return $this->limit > $value;
    }
}