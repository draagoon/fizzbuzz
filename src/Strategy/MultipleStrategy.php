<?php

declare(strict_types=1);

namespace App\Strategy;

/**
 * Interface MultipleStrategy
 * @package App\Strategy
 */
interface MultipleStrategy
{
    /**
     * @param int $value
     *
     * @return bool
     */
    public function isMultiple(int $value): bool;
}