<?php

declare(strict_types=1);

namespace App\Strategy;

/**
 * Class AllMultipleOfStrategy
 * @package App\Strategy
 */
final class AllMultipleOfStrategy implements MultipleStrategy
{
    /**
     * @var MultipleStrategy[]
     */
    private $strategies;

    /**
     * AllMultipleOfStrategy constructor.
     *
     * @param MultipleStrategy $strategy
     * @param MultipleStrategy ...$strategies
     */
    public function __construct(MultipleStrategy $strategy, MultipleStrategy... $strategies)
    {
        array_unshift($strategies, $strategy);

        $this->strategies = $strategies;
    }

    /**
     * @param int $value
     *
     * @return bool
     */
    public function isMultiple(int $value): bool
    {
        foreach ($this->strategies as $strategy) {
            if (!$strategy->isMultiple($value)) {
                return false;
            }
        }

        return true;
    }
}