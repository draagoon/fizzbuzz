<?php

declare(strict_types=1);

namespace App\Strategy;

use App\Exception\NumberException;

/**
 * Class MultipleOfStrategy
 * @package App\Strategy
 */
final class MultipleOfStrategy implements MultipleStrategy
{
    /**
     * @var int
     */
    private $value;

    /**
     * MultipleOfStrategy constructor.
     *
     * @param int $value
     */
    public function __construct(int $value)
    {
        if ($this->value === 0) {
            throw NumberException::cannotDivideByZero();
        }

        $this->value = $value;
    }

    /**
     * @param int $value
     *
     * @return bool
     */
    public function isMultiple(int $value): bool
    {
        return $value % $this->value === 0;
    }
}