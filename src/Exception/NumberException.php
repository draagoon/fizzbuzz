<?php

declare(strict_types=1);

namespace App\Exception;

use RuntimeException;

/**
 * Class NumberException
 * @package App\Exception
 */
final class NumberException extends RuntimeException
{
    public const START_IS_GREATER_THAN_LIMIT    = 'The minimum value %d is greater than the maximum value %d';
    public const CANNOT_DIVIDE_BY_ZERO          = 'The value 0 cannot be used as multiple-of value, because it is impossible to divide by 0';
    public const CANNOT_FORMAT                  = 'It is not possible to format the number %d';
    public const NEGATIVE_NUMBERS_ARENT_ALLOWED = 'Iterations with negative numbers aren\'t allowed';
    public const ENDLESS_LOOPS                  = 'Step is zero which would lead to endless loops';
    public const NO_RANGE_CAN_BE_GENERATED      = 'The minimum value and the maximum value are both %d. No range can be generated.';

    /**
     * @param int $start
     * @param int $limit
     *
     * @return self
     */
    public static function startIsGreaterThanLimit(int $start, int $limit): self
    {
        return new self(sprintf(self::START_IS_GREATER_THAN_LIMIT, $start, $limit));
    }

    /**
     * @return self
     */
    public static function cannotDivideByZero(): self
    {
        return new self(self::CANNOT_DIVIDE_BY_ZERO);
    }

    /**
     * @param int $number
     *
     * @return self
     */
    public static function cannotFormat(int $number): self
    {
        return new self(sprintf(self::CANNOT_FORMAT, $number));
    }

    /**
     * @return self
     */
    public static function negativeRangeIsNotAllowed(): self
    {
        return new self(self::NEGATIVE_NUMBERS_ARENT_ALLOWED);
    }

    /**
     * @return self
     */
    public static function stepIsZero(): self
    {
        return new self(self::ENDLESS_LOOPS);
    }

    /**
     * @param int $start
     *
     * @return self
     */
    public static function startIsEqualToLimit(int $start): self
    {
        return new self(sprintf(self::NO_RANGE_CAN_BE_GENERATED, $start));
    }
}