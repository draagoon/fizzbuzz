<?php

declare(strict_types=1);

namespace App\Formatter\Service;

use App\Generator\NumberGenerator;
use App\Formatter\NumberFormatter;

/**
 * Class NumberFormatterService
 * @package App\Formatter\Service
 */
final class NumberFormatterService
{
    /**
     * @var NumberGenerator
     */
    private $numberGenerator;
    /**
     * @var NumberFormatter
     */
    private $numberFormatter;

    /**
     * NumberPrinterService constructor.
     *
     * @param NumberGenerator $numberGenerator
     * @param NumberFormatter $numberFormatter
     */
    public function __construct(NumberGenerator $numberGenerator, NumberFormatter $numberFormatter)
    {
        $this->numberGenerator = $numberGenerator;
        $this->numberFormatter = $numberFormatter;
    }

    /**
     * @return iterable|string[]
     */
    public function formatAllGenerated(): iterable
    {
        foreach ($this->numberGenerator->generate() as $number) {
            yield $this->numberFormatter->format($number);
        }
    }
}