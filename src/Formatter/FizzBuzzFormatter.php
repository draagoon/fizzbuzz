<?php

declare(strict_types=1);

namespace App\Formatter;

/**
 * Class FizzBuzzFormatter
 * @package App\Formatter
 */
final class FizzBuzzFormatter extends AbstractNumberWordFormatter
{
    /**
     * @return string
     */
    protected function getWord(): string
    {
        return 'FizzBuzz';
    }
}