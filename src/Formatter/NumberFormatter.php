<?php

declare(strict_types=1);

namespace App\Formatter;

/**
 * Interface NumberFormatter
 * @package App\Formatter
 */
interface NumberFormatter
{
    /**
     * @param int $value
     *
     * @return string
     */
    public function format(int $value): string;
}