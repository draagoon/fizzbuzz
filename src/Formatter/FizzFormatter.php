<?php

declare(strict_types=1);

namespace App\Formatter;

/**
 * Class FizzFormatter
 * @package App\Formatter
 */
final class FizzFormatter extends AbstractNumberWordFormatter
{
    /**
     * @return string
     */
    protected function getWord(): string
    {
        return 'Fizz';
    }
}