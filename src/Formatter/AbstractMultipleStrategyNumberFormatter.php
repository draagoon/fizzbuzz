<?php

declare(strict_types=1);

namespace App\Formatter;

use App\Exception\NumberException;
use App\Strategy\MultipleStrategy;

/**
 * Class AbstractMultipleStrategyNumberFormatter
 * @package App\Formatter
 */
abstract class AbstractMultipleStrategyNumberFormatter implements NumberFormatter
{
    /**
     * @var MultipleStrategy
     */
    protected $strategy;
    /**
     * @var NumberFormatter|null
     */
    protected $nextFormatter;

    /**
     * NumberPrinter constructor.
     *
     * @param MultipleStrategy $strategy
     */
    public function __construct(MultipleStrategy $strategy)
    {
        $this->strategy = $strategy;
    }

    /**
     * @param NumberFormatter $formatter
     */
    final public function setNextFormatter(NumberFormatter $formatter): void
    {
        $this->nextFormatter = $formatter;
    }

    /**
     * @param int $number
     *
     * @return string
     */
    final protected function handleWithNextFormatter(int $number): string
    {
        if ($this->nextFormatter !== null) {
            return $this->nextFormatter->format($number);
        }

        throw NumberException::cannotFormat($number);
    }
}