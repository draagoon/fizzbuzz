<?php

declare(strict_types=1);

namespace App\Formatter;

/**
 * Class AbstractNumberWordFormatter
 * @package App\Formatter
 */
abstract class AbstractNumberWordFormatter extends AbstractMultipleStrategyNumberFormatter
{
    /**
     * @return string
     */
    abstract protected function getWord(): string;

    /**
     * @param int $number
     *
     * @return string
     */
    final public function format(int $number): string
    {
        if ($this->strategy->isMultiple($number)) {
            return $this->getWord();
        }

        return $this->handleWithNextFormatter($number);
    }
}