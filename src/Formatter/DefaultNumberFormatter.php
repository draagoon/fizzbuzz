<?php

declare(strict_types=1);

namespace App\Formatter;

/**
 * Class DefaultNumberFormatter
 * @package App\Formatter
 */
final class DefaultNumberFormatter implements NumberFormatter
{
    /**
     * @param int $number
     *
     * @return string
     */
    public function format(int $number): string
    {
        return (string)$number;
    }
}