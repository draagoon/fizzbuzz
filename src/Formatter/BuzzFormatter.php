<?php

declare(strict_types=1);

namespace App\Formatter;

/**
 * Class BuzzFormatter
 * @package App\Formatter
 */
final class BuzzFormatter extends AbstractNumberWordFormatter
{
    /**
     * @return string
     */
    protected function getWord(): string
    {
        return 'Buzz';
    }
}