DOCKER_PHP_SERVICE = php

.PHONY: new up kill run test analyse

new: kill
	docker-compose up -d --build --remove-orphans
	docker-compose exec $(DOCKER_PHP_SERVICE) composer install --optimize-autoloader

up:
	docker-compose up -d

kill:
	docker-compose kill
	docker-compose down --volumes --remove-orphans

run: up
	docker-compose exec $(DOCKER_PHP_SERVICE) php bin/console app:fizzbuzz 100 1

test: up
	docker-compose exec $(DOCKER_PHP_SERVICE) composer test

analyse: up
	docker-compose exec $(DOCKER_PHP_SERVICE) composer analyse
